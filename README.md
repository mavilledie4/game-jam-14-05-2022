# Game-Jam-14-05-2022
 
**Date :** 14-15 mai 2022   
**Thème :** Une échappatoire, "sauter" peut liberer   
**Nom du jeu :** ...  

---
## Membre de la Team :
THIRION Antoine (samedi)     
VILLEDIEU DE TORCY Mathieu      

---
## Code

**Compilation :**  `make && ./prog.exe` 

**Executable du jeu :** *prog.exe*  

---  
## But du jeu
Le but du jeu est de parcourir la distance la plus grande pour possible, pour dérouler la chaîne qui est attachée à nous. Pour briser cette chaîne et se **liberer**, il faut sauter depuis une plateforme. Plus la chaîne est déroulée, plus elle est fragile. Plus l'on saute haut, plus il est facile de briser la chaîne.   

## Info sur la structure  
Voulant faire un jeu en mode texte dans la console, je me suis tourné vers la librairie *ncurses* (sur les conseils de quelqu'un). Cependant, celle-ci ne permettant pas d'afficher des caractères spéciaux, (tel que ☺), je suis partie sur une autre méthode pour l'affichage.  
Je passe donc par un fichier dans lequel j'imprime la scène, avec les obstacles, le personnage, et la chaîne. Ce fichier est lancé dans la console pour être affiché en direct via la commande `watch -n 0,1 tail -n 50 renderer.txt.tmp`.   

## Etat des lieux  
Seulement les déplacements du personnage dans une carte sont codés. Il reste un soucis lors du saut du personnage qui peut traverser certaines plateforme.  
Un début de chaîne a été implémenté, cependant il reste tout la partie permettant de "tendre" la chaîne pour qu'elle suive le trajet du personnage.   
La mécanique de hauteur de saut, ainsi que la fragilité de la chaîne suivant sa longueur ne sont pas présent.   

## Doc utile pendant la jam  

Documentation ncurses : [lien](https://arnaud-feltz.developpez.com/tutoriels/ncurses/?page=premier_pas)   
Lecture/écriture fichier cpp : [lien](http://sdz.tdct.org/sdz/lecture-et-ecriture-dans-les-fichiers-en-c.html)   

---  
## Repas du week-end
### Repas du samedi midi :
Repas au [RU](https://usine.crous-clermont..fr/restaurant/resto-u-restocezo/) 

### Repas du samedi soir
Pizza commandée par l'équipe Pixel au [Milano Pizza](https://milano-pizza-63-71.webself.net/)
  
Mathieu   : Jambon Cru           (Large, base tomate)   
Axel      : Chorizon             (Large, base tomate)   
Alexandre : Chèvre, poulet, miel (Large, base crème)    

**RQ** : Pizzas n'étaient pas prêtes à 20h04.   

### Repas du dimanche midi : [BDE ISIMA](https://bde.isima.fr/)
 
Mathieu   : Sandwich Jambon beurre x2 + chips    
Axel      : Pastabox bolognaise            
Alexandre : Sandwich Jambon beurre x2   
 
Dessert : Petite part de gateau au chocolat très bon. (Merci Isibouffe)   

**RQ:** : Il y avait du vent devant le BDE, nos dechets s'envolait mais comme on est trop fort on a tous rattrapé.   

---
## Évènements divers   


- Changement de salle le samedi après-midi, juste après le repas.   
- Arrivée d'Alexandre et Axel uniquement l'après-midi le samedi.  
- Reception du mail de la semaine à 20h05.  
- Premier ménage de la team BDE après une soirée off, 18h39 sol encore sale et comptoir en bazar. (Ils sont pas très efficace)     

