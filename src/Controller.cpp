#include "Controller.hpp"

Controller *Controller::_singleton = nullptr;
Time Controller::tickTime = milliseconds(100);

Controller::Controller()
    : map{nullptr},
      perso{new Perso(1, 2)},
      _renderer{nullptr},
      _clock{},
      _chaine{perso}
{
}

Controller::~Controller()
{
    delete map;
    delete perso;
    _renderer->close();
}

Controller *Controller::getSingleton()
{
    return (_singleton == nullptr ? new Controller{} : _singleton);
}

int Controller::niveau(string nomFichierLevel)
{
    map = new Map{nomFichierLevel};
    _chaine.init(map->getLignes(), map->getColonnes());

    chargerMapRenderer();

    Vector2i inputDirX{44, 2};
    Vector2i inputDirY{44, 2};
    Vector2i inputDir{44, 2};

    Vector2i vectGravite{0, 1};
    int gravite = 1;

    int inputX, inputY;

    cout << Map::vect2DToPosFile(inputDir) << endl;

    while (1)
    {
        recuperationInput(inputX, inputY);
        if (_clock.getElapsedTime() >= tickTime)
        {
            // Deplace horizontalement
            if (!map->checkCollision(perso->getPlaceOccupeeToDirection(Vector2i(inputX, 0))))
                perso->deplacer(Vector2i(inputX, 0));
            chargerRenderer();

            // Test case en dessous perso
            if (map->checkCollision(perso->getPlaceOccupeeToDirection(vectGravite)))
            { // Oui
                if (map->checkCollision(perso->getPlaceOccupeeToDirection(-1 * vectGravite)))
                {
                    perso->setAccelerationY(0);
                    inputY = 0;
                }
            }
            else
            { // Non
                inputY = gravite;
            }
            perso->deplacer(Vector2i(0, inputY));

            _chaine.update();
            chargerRenderer();

            _clock.restart();
        }

        // printf("direction : (%d, %d)\n", inputDir.x, inputDir.y);
    }

    return 0;
}

void Controller::chargerMapRenderer()
{
    // map->chargerCarte("ressources/map.txt");

    // Create un fichier pour le renderer
    // Ecrase celui existant
    _renderer = new fstream("renderer.txt.tmp",
                            ios::out | ios::trunc);

    // Verif que le flux a bien ete ouvert
    // et sauvegarde du flux dans renderer
    if (!_renderer->is_open())
        cerr << "Erreur dans la creation du renderer.txt.tmp" << endl;

    map->afficherOStream(*_renderer);
    _renderer->flush();
}

void Controller::chargerPersoRenderer()
{
    perso->effacer(*_renderer);
    perso->afficher(*_renderer);
    _renderer->flush();
}

void Controller::chargerChaineRenderer()
{
    _chaine.afficher(*_renderer);
}

void Controller::chargerRenderer()
{

    // chargerMapRenderer();

    chargerChaineRenderer();
    chargerPersoRenderer();
    _renderer->flush();
}

/**
 * @brief Regarde les entrees clavier et set le vecteur de direction.
 * 
 * @param Vector2i & - *inputDir*
 */
void Controller::recuperationInput(int &inputX, int &inputY)
{
    // Horizontal
    inputX = -1 * Keyboard::isKeyPressed(Keyboard::Left) +
             1 * Keyboard::isKeyPressed(Keyboard::Right);

    // Vertical
    inputY = 0 * Keyboard::isKeyPressed(Keyboard::Down) +
             -1 * (Keyboard::isKeyPressed(Keyboard::Up) ||
                   Keyboard::isKeyPressed(Keyboard::Space));
}