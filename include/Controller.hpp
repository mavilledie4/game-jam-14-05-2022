#ifndef __CONTROLLER_HPP__
#define __CONTROLLER_HPP__

#include <fstream>
#include <string>
#include <iostream>

#include "Map.hpp"
#include "Perso.hpp"
#include "enumInput.hpp"
#include "Chaine.hpp"

#include <SFML/Window/Keyboard.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/System/Clock.hpp>

using namespace sf;
using namespace std;

class Controller
{
private:
    Map *map;
    Perso *perso;
    fstream *_renderer;
    Clock _clock;
    Chaine _chaine;


private: // Static
    static Controller *_singleton;
static Time tickTime;

public: // Static
    static Controller *getSingleton();

private:
    Controller();

public:
    ~Controller();

    int niveau(string nomFichierLevel);
    void chargerMapRenderer();

    void chargerPersoRenderer();
    void chargerChaineRenderer();

    void chargerRenderer();

    void recuperationInput(int &inputX,int &inputY);
};

#endif