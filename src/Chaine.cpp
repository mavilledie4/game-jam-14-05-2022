#include "Chaine.hpp"

Chaine::Chaine(Perso *perso)
    : _tab{nullptr},
      _lignes(0), _colonnes(0),
      _perso(perso),
      _origine{perso->getPositionNew()}
{
}

Chaine::~Chaine() {}

void Chaine::init(int lignes, int colonnes)
{
    _lignes = lignes;
    _colonnes = colonnes;

    // Tab
    _tab = new char *[_lignes];
    for (int i = 0; i < _lignes; ++i)
        _tab[i] = new char[_colonnes];

    _tab[_origine.y][_origine.x] = '-';

    for (int i = 0; i < _lignes; i++)
    {
        for (int j = 0; j < _colonnes; ++j)
        {
            _tab[i][i] = ' ';
        }
    }

    // Tab Bis tmp
    _tabBis = new char *[_lignes];
    for (int i = 0; i < _lignes; ++i)
        _tabBis[i] = new char[_colonnes];

    for (int i = 0; i < _lignes; i++)
    {
        for (int j = 0; j < _colonnes; ++j)
        {
            _tabBis[i][j] = ' ';
        }
    }
}

void Chaine::update()
{
    Vector2i diff;
    Vector2i posPersoNew = _perso->getPositionNew() + _perso->getPointAccroche();
    Vector2i posPersoOld = _perso->getPositionOld() + _perso->getPointAccroche();

    _origine = _perso->getPositionOld();

    diff.x = posPersoNew.x - posPersoOld.x;
    diff.y = posPersoNew.y - posPersoOld.y;

    if (diff.x != 0 && diff.y == 0)
    {
        _tab[posPersoOld.y][posPersoOld.x] = '-';
    }
    else if (diff.x == 0 && diff.y != 0)
    {
        _tab[posPersoOld.y][posPersoOld.x] = '|';
    }
    else if ((diff.x < 0 && diff.y < 0) ||
             (diff.x > 0 && diff.y > 0))
    {
        _tab[posPersoOld.y][posPersoOld.x] = '\\';
    }
    else if ((diff.x > 0 && diff.y < 0) ||
             (diff.x < 0 && diff.y > 0))
    {
        _tab[posPersoOld.y][posPersoOld.x] = '/';
    }
    _tab[posPersoOld.y][posPersoOld.x] = '*';
}

void Chaine::afficher(fstream &flux)
{
    char c;
    int posFichier;
    for (int i = 0; i < _lignes; i++)
    {
        for (int j = 0; j < _colonnes; ++j)
        {
            c = _tab[i][j];
            if (c == '*' || c == '-' || c == '/' || c == '\\' || c == '|')
            {
                _origine.x = j;
                _origine.y = i;
                posFichier = Map::vect2DToPosFile(_origine);
                flux.seekp(posFichier);
                flux.put(c);
            }
        }
    }
}

int Chaine::compte()
{
    int compteur = 0;
    for (int i = 0; i < _lignes; i++)
    {
        for (int j = 0; j < _colonnes; ++j)
        {
            compteur += (_tab[i][j] == '*');
        }
    }
    return compteur;
}
