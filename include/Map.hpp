#ifndef __MAP_HPP__
#define __MAP_HPP__

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>

#include <list>

#include <SFML/System/Vector2.hpp>

using namespace std;
using namespace sf;

class Map
{
private:
    char **_carte;
    // Perso perso;

public: // Static
    static int LIGNES;
    static int COLONNES;

public: // Static
    static const int getLignes() { return LIGNES; }
    static const int getColonnes() { return COLONNES; }
    static int vect2DToPosFile(Vector2i &posVect2D);

public:
    Map(string nomFichier);
    ~Map();
    int **afficher(string);

    bool checkCollision(Vector2i &pointVerif);
    bool checkCollision(list<Vector2i> listointVerif);

    void chargerCarte(string nomFichier);
    void afficherOStream(ostream &flux = cout);
};

/***************************************************/
/*                 Méthodes inline                 */
/***************************************************/

/***************************************************/
/*              Méthodes inline static             */
/***************************************************/
inline int Map::vect2DToPosFile(Vector2i &posVect2D)
{
    if (posVect2D.x >= 0 && posVect2D.x < COLONNES &&
        posVect2D.y >= 0 && posVect2D.y < LIGNES)
        return posVect2D.x + posVect2D.y * (COLONNES + 1);
    else
        return 0;
}

/***************************************************/
/*           Méthodes inline non static            */
/***************************************************/

#endif