#include "Map.hpp"

int Map::LIGNES = 0;
int Map::COLONNES = 0;

Map::Map(string nomFichier)
    : _carte(nullptr)
{
    chargerCarte(nomFichier);
}

Map::~Map()
{
    for (int k = 0; k < LIGNES; ++k)
        delete[] _carte[k];
    delete[] _carte;
}

/*
int **Map::afficher(string NomFichier)
{
    string line;

    ifstream input_file(NomFichier, ios::in);
    LIGNES = getline(input_file, line);
    COLONNES = getline(input_file, line);
    int mat[LIGNES][COLONNES];
    int i = 0;

    while (getline(input_file, line))
    {
        printw("**"); // Affiche la ligne line dans la console
        refresh();    // Rafraîchit la fenêtre courante afin de voir le message apparaître
        line = line.c_str();
        for (int j = 0; j < COLONNES; j++)
        {
            mat[i][j] = line[j];
        }
        i++;
    }
    input_file.close();
    return mat;
}
*/

/**
 * @brief Charge les obstacles de la carte dans la matrice.
 * 
 * @details 
 * Lit le fichier et set les variables
 * LIGNES et COLONNES qui definissent la tailles
 * de la carte. Pui instancie une matrice de char
 * et set la matrice de char correspondant 
 * aux obstacles de la carte.
 * 
 * @param string - *nomFichier*
 */
void Map::chargerCarte(string nomFichier)
{
    // Ouverture du fichier de la carte en lecture seule
    ifstream monFlux(nomFichier.c_str());

    if (monFlux.is_open())
    {
        // lecture des dimensions de la carte
        monFlux >> LIGNES;
        monFlux >> COLONNES;

        // Instancie la matrice de la carte vide
        _carte = new char *[LIGNES];
        for (int k = 0; k < LIGNES; ++k)
            _carte[k] = new char[COLONNES];

        char cara;

        // Lecture et remplissage des obstacles de la carte
        for (int i = 0; i < LIGNES; i++)
        {
            for (int j = 0; j < COLONNES; j++)
            {
                do
                {
                    monFlux.get(cara);
                    _carte[i][j] = cara;
                } while (cara == '\n');
                // cout << _carte[i][j];
            }
            // cout << endl;
        }

        monFlux.close();
    }
    else
    {
        cerr << "Erreur d'ouverture du fichier : " + nomFichier << endl;
    }
}

/**
 * @brief Affiche la carte dans un flux.
 * 
 * @param flux 
 */
void Map::afficherOStream(ostream &flux)
{
    // Affichage dans la console
    for (int i = 0; i < LIGNES; i++)
    {
        for (int j = 0; j < COLONNES; j++)
        {
            flux << (char)_carte[i][j];
        }
        flux << endl;
    }
}

/**
 * @brief Verifie si le point est sur un obstacle.
 * 
 * @param const Vector2i & - *pointVerif*
 * @return true - *collision avec un obstacle*
 * @return false - *pas de collision*
 */
bool Map::checkCollision(Vector2i &pointVerif)
{
    if (pointVerif.x >= 0 && pointVerif.x < COLONNES &&
        pointVerif.y >= 0 && pointVerif.y < LIGNES)
    {
        return !(_carte[pointVerif.y][pointVerif.x] == ' ');
    }
    else
    {
        cerr << "Erreur d'indice dans le point a verifier" << endl;
        return true;
    }
}

/**
 * @brief Verifie si la liste des points n'est pas sur un obstacle.
 * 
 * @param listPointVerif 
 * @return true - *collision d'un point avec un obstacle*
 * @return false - *aucune collision*
 */
bool Map::checkCollision(list<Vector2i> listPointVerif)
{
    bool coll = false;
    for (auto pt : listPointVerif)
    {
        coll = coll || checkCollision(pt);
    }
    return coll;
}
