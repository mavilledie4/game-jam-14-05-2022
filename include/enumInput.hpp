#ifndef __ENUMINPUT_HPP__
#define __ENUMINPUT_HPP__

enum inputKey 
{
    flecheGauche = 68,
    flecheDroite = 67,
    espace = 32
};

#endif