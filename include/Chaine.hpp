#ifndef __CHAINE_HPP__
#define __CHAINE_HPP__

#include "Perso.hpp"
#include <SFML/System/Vector2.hpp>

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <list>
#include <vector>
#include <algorithm>
#include <iterator>
#include <iomanip>

#include "Map.hpp"

using namespace std;
using namespace sf;

class Chaine
{
private:
    char **_tab;
    char **_tabBis;
    int _lignes;
    int _colonnes;
    Perso *_perso;
    Vector2i _origine;

public:
    Chaine(Perso *perso);
    ~Chaine();

    void init(int lignes, int colonnes);

    void update();
    void afficher(fstream &flux);

    int compte();

};

#endif