#include "Perso.hpp"

// ┼ ☺ ☻

// char Perso::_sprite[3][1] = {{(char)'☻'},
//  {(char)'□'},
//  {(char)'^'}};
char Perso::_sprite[3][1] = {{(char)'o'},
                             {(char)'T'},
                             {(char)'M'}};

int Perso::_saut = 5;

Perso::Perso(const int x, const int y) : Perso{Vector2i(x, y)} {}

Perso::Perso(const Vector2i &posInitial)
    : _positionNew(posInitial), // /!\ => (x, y)
      _positionOld(posInitial), // /!\ => (x, y)
      _width(1), _height(3),
      _accelerationY(1),
      _pointAccroche(0, 1)
{
}

Perso::~Perso() {}

/***************************************************/

/**
 * @brief Deplace le personnage (nouvelle position valide)
 * 
 * @param const Vector2i & - *direction*
 */
void Perso::deplacer(const Vector2i &direction)
{
    if (!(direction == Vector2i(0, 0)))
        _positionOld = _positionNew;

    // Deplacement horizontal
    _positionNew.x += direction.x;


    // Set acceleration vertical
    if (direction.y < 0 && _accelerationY >= 0)
    {
        _accelerationY = -_saut;
    }
    else if (direction.y > 0)
    {
        _accelerationY++;
    }

    // Deplacement verticale
    if (_accelerationY < 0)
    {
        _positionNew.y -= 1;
        _accelerationY++;
    }
    else if (_accelerationY > 0)
    {
        _positionNew.y += 1;
        _accelerationY--;
    }
}

void Perso::afficher(fstream &flux)
{
    int posFichier;
    Vector2i vectCase;


    // Personnage
    for (int i = 0; i < _height; i++)
    {
        for (int j = 0; j < _width; j++)
        {
            vectCase = _positionNew + Vector2i(j, i);
            posFichier = Map::vect2DToPosFile(vectCase);
            flux.seekp(posFichier);
            flux.put(_sprite[i][j]);
        }
    }
}

void Perso::effacer(fstream &flux)
{
    int posFichier;
    Vector2i vectCase;

    for (int i = 0; i < _height; i++)
    {
        for (int j = 0; j < _width; j++)
        {
            vectCase = _positionOld + Vector2i(j, i);
            posFichier = Map::vect2DToPosFile(vectCase);
            flux.seekp(posFichier);
            flux.put(' ');
        }
    }
}

/**
 * @brief  Donne la liste des points occupes
 * 
 * @return list<Vector2i> 
 */
list<Vector2i> Perso::getPlaceOccupee() const
{
    return getPlaceOccupeeToDirection(Vector2i{0, 0});
}

/**
 * @brief Donne la liste des points occupes par le perso
 * s'il se deplace dans une direction
 * 
 * @param const Vector2i & - *dir* 
 * @return list<Vector2i> 
 */
list<Vector2i> Perso::getPlaceOccupeeToDirection(const Vector2i &dir) const
{
    list<Vector2i> listPos;
    Vector2i posCal;
    for (int i = 0; i < _height; i++)
    {
        for (int j = 0; j < _width; j++)
        {
            posCal = _positionNew + Vector2i(j, i) + dir;
            listPos.push_back(posCal);
        }
    }
    return listPos;
}
