#ifndef __PERSO_HPP__
#define __PERSO_HPP__

#include <SFML/System/Vector2.hpp>

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <list>
#include <vector>
#include <algorithm>
#include <iterator>
#include <iomanip>

#include "Map.hpp"

using namespace std;
using namespace sf;



class Perso
{
private:
    Vector2i _positionNew; // /!\ => (x, y) => position de la tete
    Vector2i _positionOld; // Ancienne posistion perso avant deplacement
    int _width;            // = 1 => peut etre const ?
    int _height;           // = 3 => peut etre const ?
    int _accelerationY;    // = 1 ? a tester
    Vector2i _pointAccroche;


private: // Static
    static char _sprite[3][1];
    static int _saut;

public: // Static
    int getSaut();

public:
    Perso(const int x, const int y);
    Perso(const Vector2i &posInitial = Vector2i(0, 0));
    ~Perso();

    void deplacer(const Vector2i &direction = Vector2i(0, 0));
    void afficher(fstream &flux);
    void effacer(fstream &flux);

    // Getter
    const Vector2i &getPositionNew() const; // /!\ => (y, x)
    const Vector2i &getPositionOld() const; // /!\ => (y, x)
    const Vector2i &getPointAccroche() const;
    const int getHeight() const;
    const int getWidth() const;
    const int getAccelerationY() const;
    list<Vector2i> getPlaceOccupee() const;
    list<Vector2i> getPlaceOccupeeToDirection(const Vector2i &dir) const;

    // Setter
    void setAccelerationY(int val);
};

/***************************************************/
/*                 Méthodes inline                 */
/***************************************************/

/***************************************************/
/*              Méthodes inline static             */
/***************************************************/
inline int Perso::getSaut() { return _saut; }

/***************************************************/
/*           Méthodes inline non static            */
/***************************************************/
inline const Vector2i &Perso::getPositionNew() const { return _positionNew; }
inline const Vector2i &Perso::getPositionOld() const { return _positionOld; }
inline const Vector2i &Perso::getPointAccroche() const { return _pointAccroche; }
inline const int Perso::getHeight() const { return _height; }
inline const int Perso::getWidth() const { return _width; }
inline const int Perso::getAccelerationY() const { return _accelerationY; }

inline void Perso::setAccelerationY(int val) { _accelerationY = val; }

#endif