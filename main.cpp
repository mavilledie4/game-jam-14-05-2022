#define _X_OPEN_SOURCE_EXTENDED

#include <string>
#include <cstring>
#include <iostream>
#include <fstream>
#include <thread>
#include <ncurses.h>
#include "Perso.hpp"

#include "Map.hpp"
#include "Controller.hpp"

using namespace std;

const string cmdWatchTail = "watch -n 0,1 tail -n 50 ";
const string nomFichier = "renderer.txt.tmp";
const string cmd = cmdWatchTail + nomFichier;

void testMap();
void testController();
void initRenderer();
// void testTime();

int main()
{

    // initRenderer();

    testController();
 

    // testMap();

    return 0;
}

void testMap()
{
    string nomFichier = "ressources/map.txt";

    Map *map = new Map{nomFichier};
    cout << "nb lignes : " << map->getLignes() << endl;
    cout << "nb colonnes : " << map->getColonnes() << endl;
    map->afficherOStream();

    delete map;
}

void testController()
{

    string nomFichier = "ressources/map.txt";

    Controller *controller = Controller::getSingleton();

    controller->niveau(nomFichier);

    delete controller;
}

void initRenderer()
{
    system(cmd.c_str());
}
